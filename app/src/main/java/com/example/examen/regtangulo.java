package com.example.examen;

import java.io.Serializable;

public class Regtangulo implements Serializable {

    private int base;
    private int altura;

    public RegtanguloActivity(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }

    public RegtanguloActivity() {

    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public float calcularArea(){
        float area = this.base*this.altura;
        return area;
    }
    public float calcularPerimetro(){
        float perimetro = (this.altura*2)+(this.base*2);
        return perimetro;
    }
}
